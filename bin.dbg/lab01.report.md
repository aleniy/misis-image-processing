## Работа 1. Исследование гамма-коррекции
автор: Шилина А.М.
дата: 2020-05-06T01:00:17

<!-- url: https://gitlab.com/aleniy/misis-image-processing/-/tree/master/prj.lab/lab01 -->

### Задание
1. Сгенерировать серое тестовое изображение $I_1$ в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению $I_1$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_1$.
3. Сгенерировать серое тестовое изображение $I_2$ в виде прямоугольника размером 768х60 пикселя со ступенчатым изменением яркости от черного к белому (от уровня 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
4. Применить  к изображению $I_2$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_2$.
5. Показать визуализацию результатов в виде одного изображения, об

### Результаты

![](lab01.png)
Рис. 1. Результаты работы программы (сверху вниз $I_1$, $G_1$, $I_2$, $G_2$)

### Текст программы

```cpp
#include <opencv2/opencv.hpp>

//using namespace cv;

int main() {
 
  int w = 768;
  int h = 60;
  double gamma = 2.2;

  cv::Mat rec(cv::Mat::zeros(h, w, CV_8UC1));
  for (int i = 0; i < rec.rows; i++) {
	  for (int j = 0; j < rec.cols; j++) { 
		  rec.at<uint8_t>(i, j) = j / 3; 
	  }
  }
  cv::Mat gam_cor;
  rec.convertTo(gam_cor, CV_32FC1, 1 / 255.0);
  cv::pow(gam_cor, gamma, gam_cor);
  gam_cor.convertTo(gam_cor, CV_8UC1, 255);
  cv::Mat res;
  cv::vconcat(rec, gam_cor, res);
  for (int i = 0; i < rec.rows; i++) {
	  for (int j = 0; j < rec.cols; j++) {
		  rec.at<uint8_t>(i, j) = j / 30 * 10 + 5;
	  }
  }
  cv::vconcat(res, rec, res);
  rec.convertTo(gam_cor, CV_32FC1, 1 / 255.0);
  cv::pow(gam_cor, gamma, gam_cor);
  gam_cor.convertTo(gam_cor, CV_8UC1, 255);
  cv::vconcat(res, gam_cor, res);
  cv::imwrite("lab01.png", res);
}

```
