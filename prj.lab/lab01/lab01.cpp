#include <opencv2/opencv.hpp>

int main() {
 
  int w = 768;
  int h = 60;
  double gamma = 2.2;

  cv::Mat rec(cv::Mat::zeros(h, w, CV_8UC1));
  for (int i = 0; i < rec.rows; i++) {
	  for (int j = 0; j < rec.cols; j++) { 
		  rec.at<uint8_t>(i, j) = j / 3; 
	  }
  }
  cv::Mat gam_cor;
  rec.convertTo(gam_cor, CV_32FC1, 1 / 255.0);
  cv::pow(gam_cor, gamma, gam_cor);
  gam_cor.convertTo(gam_cor, CV_8UC1, 255);
  cv::Mat res;
  cv::vconcat(rec, gam_cor, res);
  for (int i = 0; i < rec.rows; i++) {
	  for (int j = 0; j < rec.cols; j++) {
		  rec.at<uint8_t>(i, j) = j / 30 * 10 + 5;
	  }
  }
  cv::vconcat(res, rec, res);
  rec.convertTo(gam_cor, CV_32FC1, 1 / 255.0);
  cv::pow(gam_cor, gamma, gam_cor);
  gam_cor.convertTo(gam_cor, CV_8UC1, 255);
  cv::vconcat(res, gam_cor, res);
  cv::imwrite("lab01.png", res);
}
