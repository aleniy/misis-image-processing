## Работа 4. Использование частных производных для выделения границ
автор: Полевой Д.В.
url: https://gitlab.com/2020-misis-spring/polevoi_d_v

### Задание
1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата $F_1$.
3. Применить второй линейный фильтр и сделать визуализацию результата $F_2$.
4. Вычислить $R=\sqrt{F_1^2 + F_2^2}$  и сделать визуализацию R.

### Результаты

![](lab04.src.png)
Рис. 1. Исходное тестовое изображение

![](lab04.viz_dx.png)
Рис. 2. Визуализация результата $F_1$ применения фильтра

![](lab04.viz_dy.png)
Рис. 3. Визуализация результата $F_2$ применения фильтра

![](lab04.viz_gradmod.png)
Рис. 4. Визуализация модуля градиента $R$

### Текст программы

```cpp
#include <opencv2/opencv.hpp>

static const int d = 100;
static const cv::Rect2i rc_cell{0, 0, d, d};

void draw_cell(cv::Mat& img, const int32_t x, const int32_t y, 
  const cv::Scalar color_ground, const cv::Scalar color_figure) {
  cv::Rect2i rc = rc_cell;
  rc.x += x;
  rc.y += y;
  cv::rectangle(img, rc, color_ground, -1);
  cv::circle(img, {rc.x + rc.width / 2, rc.y + rc.height / 2}, rc.width / 5, color_figure, -1);
}

int main() {
  cv::Mat img_src(200, 300, CV_8UC1);
  cv::Mat img_res = img_src.clone();

  // draw source image
  draw_cell(img_src, 0, 0, { 255 }, { 127 });
  draw_cell(img_src, d, 0, { 127 }, { 0 });
  draw_cell(img_src, d * 2, 0, { 0 }, { 255 });
  draw_cell(img_src, 0, d, { 0 }, { 127 });
  draw_cell(img_src, d, d, { 255 }, { 0 });
  draw_cell(img_src, d * 2, d, { 127 }, { 255 });
  cv::imwrite("lab04.src.png", img_src);
  
  cv::imwrite("lab04.viz_dx.png", img_res);
  cv::imwrite("lab04.viz_dy.png", img_res);
  cv::imwrite("lab04.viz_gradmod.png", img_res);
}
```
